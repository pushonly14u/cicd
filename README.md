# License and Copyright info Automation Prototype


This project is a prototype for automating the License and Copyright information for third-party libraries used in our projects.
### local setup
   ```sh
- Install docker in system
- docker pull ubuntu
- docker run -it --name my_ubuntu -p 80:80 ubuntu
- apt update && apt upgrade
- apt install default-jre -y
- apt install maven -y    
- apt install python3 -y    
- apt install python3-pip -y  
- pip3 install scancode-toolkit
- apt install gitlab-runner
- register your gitlab-runner 
- apt install nginx && nginx
- apt install sudo 
- add 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' at the end in file /etc/sudoers 
   ```

Now Push our code it's done. you can access license info on-
   ```sh
    localhost:80/reportapp.html
    localhost:80/report.html or .json or .csv
   ```
      